#!/usr/bin/env/python3

import random

"""Creation of the character class to be used by both the Hero and Monster"""
class Character:
    def __init__(self):
        self._inventory = []
    
    def initiative(self):
        self._initiative = random.randrange(1,7)
        return self._initiative
    
    """Returns the inventory"""
    def inventory(self):
        return self._inventory
        
"""Hero class that inherets from character, contains attack"""
class Hero(Character):
    def __init__(self, health = 10):
        self._health = health
        self._inventory = {}
        
    def attack(self, other, decision = "nothing"):
        if decision == "nothing":
            attacker = []
            defender = []
            for attack in range(3):
                attacker.append(random.randrange(1,7))
            attacker.sort()
            attack = attacker[-1]
            for defend in range(other._strength):
                defender.append(random.randrange(1,7))
            defender.sort()
            defend = defender[-1]
            if attack >= defend:
                other._health -= 1
                return 1
        try:        
            if self._inventory[int(decision)] == "Sword of Omens":
                reply = input("You hear a voice saying to shout out the battlecry of the Thunder Cats: ")
                if reply != "thunder thunder thunder thundercats ho!":
                    return 2
                else:
                    other._health = 0
                    return 1
            elif self._inventory[int(decision)] == "+1 Attack Potion":
                del self._inventory[int(decision)]
                attacker = []
                defender = []
                for attack in range(3):
                    attacker.append(random.randrange(1,7))
                attacker.sort()
                attack = attacker[-1]
                for defend in range(other._strength):
                    defender.append(random.randrange(1,7))
                defender.sort()
                defend = defender[-1]
                if attack+1 >= defend:
                    other._health -= 1
                    return 1
            elif self._inventory[int(decision)] == "Holy Hand Grenade of Antioch":
                del self._inventory[int(decision)]
                reply = input("You pull the pin and count: ")
                if reply != "1 2 3":
                    return 0
                else:
                    other._health = 0
                    return 1
        except:
            self.attack(other)

"""Monster class that inherets from character, contains attack"""
class Monster(Character):
    def __init__(self):
        self._health = random.randrange(1,4)
        self._strength = random.randrange(1,4)
        
    def attack(self, other):
        attacker = []
        for attack in range(self._strength):
            attacker.append(random.randrange(1,7))
        attacker.sort()
        attack = attacker[-1]
        defender = []
        for defend in range(3):
            defender.append(random.randrange(1,7))
        defender.sort()
        defend = defender[-1]
        if attack >= defend:
            other._health -= 1
            return 1
        