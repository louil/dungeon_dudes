#!/usr/bin/env/python3

import dungeon_dudes_classes as d
import random
import time
import os
random.seed(time.time())

"""Function that takes the required variables to play the game and plays for one floor, upon selecting to move location"""
"""will break out and return to main"""
def play_game(Hero, Monster, treasure, loot, current_floor, floor_limit):
    while True:
        if Hero._health == 0:
            print("You have been slain!")
            exit()
        if Monster._health == 0 and current_floor in loot.keys():
            print("You have found loot! Open your inventory to see what you got.\n")
            Hero._inventory[len(Hero._inventory)] = loot[current_floor]
            del loot[current_floor]
        print("What would you like to do:")
        print("1. See your inventory.")
        print("2. Move to next location.")
        print("3. See your health.")
        print("4. See enemy health.")
        print("5. Attack!")
        choice = input("Make your choice...: ")
        if choice == '1':
            print()
            items = Hero.inventory()
            print("         INVENTORY         ")
            print("---------------------------")
            for item, key in items.items():
                print(item,":",key)
            print("---------------------------")
        elif choice == '2':
            if Monster._health > 0: 
                print("\nYou can not continue without defeating the creature before you!\n")
                continue
            else:
                current_floor += 1
                if current_floor < floor_limit:
                    print("\nYou have entered another level, similar to the ones before.\n")
                    break
                elif current_floor == floor_limit:
                    print("\nYou have entered the final floor.\n")
                    break
                elif current_floor > floor_limit:
                    print("\nYou have cleared The Darkest Dungeon!")
                    print("But can you resist the temptation for more... Only time will tell.\n")
                    break
        elif choice == '3':
            print("\nHealth remaining is:", Hero._health)
            print()
        elif choice == '4':
            print("\nHealth remaining is:", Monster._health)
            print()
        elif choice == '5':
            if Monster._health == 0:
                print("\nYou are trying to attack a dead body... you sicko.\n")
                continue
            if len(Hero.inventory()) != 0:
                items = Hero.inventory()
                inventory_choice = input("Would you like to equip/use an item?(yes/no):")
                if inventory_choice == "yes":
                    print("         INVENTORY         ")
                    print("---------------------------")
                    for item, key in items.items():
                        print(item,":",key)
                    print("---------------------------")
                    try:
                        decision = int(input("Choose wisely: "))
                        if decision == item in items.keys():
                            pass
                    except:
                        decision = "nothing"
                    attack = Hero.attack(Monster, decision)
                    if attack == 0:
                        print("\nOh god you can't count to three!")
                        print("BOOM!")
                        print("...You have been slain.")
                        exit()
                    if attack == 1:
                        print("\nHero's attack hit!")
                        if Monster._health < 1:
                            print("The Hero has slain the Monster!\n")
                    elif attack == 2:
                        print("\nMaybe you should have watched more television as a kid!")
                    else:
                        print("\nHero's attack failed!")
                else:
                    decision = "nothing"
                    attack = Hero.attack(Monster, decision)
                    if attack == 1:
                        print("\nHero's attack hit!")
                        if Monster._health < 1:
                            print("The Hero has slain the Monster!\n")
                    else:
                        print("\nHero's attack failed!")
            else:
                decision = "nothing"
                attack = Hero.attack(Monster, decision)
                if attack == 1:
                    print("\nHero's attack hit!")
                    if Monster._health < 1:
                        print("The Hero has slain the Monster!\n")
                else:
                    print("\nHero's attack failed")
            if Monster._health > 0:
                print("The Monster attacks!")
                if Monster.attack(Hero) == 1:
                    print("Monsters's attack hit!\n")
                else:
                    print("Monster's attack failed!\n")
        else:
            os.system("clear")

"""The main function of the program, creates the neccessary variables to be passed to the play_game function,"""
"""the function loops a total of 10 times exiting upon clearing the 10th floor or if the Hero dies"""
def main():
    print("Welcome to... The Darkest Dungeon!!!\n")
    
    Hero = d.Hero()
    
    treasure = random.randrange(1,10)
    treasure2 = random.randrange(1,10)
    treasure3 = random.randrange(1,10)

    loot = {treasure:"Sword of Omens", treasure2:"+1 Attack Potion", treasure3:"Holy Hand Grenade of Antioch"}
    
    print("Let us begin the descent into the realm of madness.")
    print("You have entered the first floor, by way of the entrance.")
    print("What appears before you in the dimly lit surroundings is a pathway to a door.")
    print("The door seems to lead to a lower level, however a dark figure appears before you!\n")
    
    current_floor = 1
    floor_limit = 10
    while current_floor <= floor_limit:
        Monster = d.Monster()
        if Hero.initiative() >= Monster.initiative():
            play_game(Hero, Monster, treasure, loot, current_floor, floor_limit)
            current_floor += 1
        else:
            Monster.attack(Hero)
            print("The Monster launches a surprise attack!")
            if Monster.attack(Hero) == 1:
                print("Monsters's attack hit!\n")
            else:
                print("Monster's attack failed!\n")
            play_game(Hero, Monster, treasure, loot, current_floor, floor_limit)
            current_floor += 1
                    
    
if __name__ == "__main__":
    main()